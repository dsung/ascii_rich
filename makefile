CXX=g++
main: main.o
	$(CXX) -o $@ $< -lncursesw 
main.o: main.cpp
	$(CXX) -g -c $<
t0: t0.cpp
	$(CXX) -g -o $@ $< -lncursesw 
	
clean:
	rm -rf *.o main
