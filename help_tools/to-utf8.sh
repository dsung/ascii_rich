#!/bin/sh
cp $1 $1.tmp
iconv -f big5 -t utf8 < $1.tmp > $1
mv $1.tmp $1.big5
