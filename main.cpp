/*
by hu7592
*/
#include <unistd.h>
#include <ncurses.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
//#include <windows.h>
#include <string.h>
#include <math.h>
#include <time.h>
/*按鍵內碼*/
#define UP KEY_UP
#define DOWN KEY_DOWN
#define ENTER 10
#define LEFT KEY_LEFT
#define RIGHT KEY_RIGHT
#define HOME KEY_HOME
#define END KEY_END
//#define DELETE 83
//#define INSERT 82
#define F12 KEY_F(12)
#define F4 KEY_F(4)
#define F9 KEY_F(9)
#define F11 KEY_F(11)
#define F8 KEY_F(8)
#define F7 KEY_F(7)
/*選單極大/小值*/
#define MENU_FIRST 1
#define MENU_LAST 5
#define MAP_FIRST 1
#define MAP_LAST 2
#define PEOPE_FIRST 1
#define PEOPE_LAST 5
#define COMPUTER_FIRST 0
#define COMPUTER_LAST 4
#define MONEY_FIRST 1
#define MONEY_LAST 5
#define MODE_FIRST 1
#define MODE_LAST 2
#define NAMELIST 5
#define TC_NONE 0
/*顏色定義 0 黑色  1 紅色  3黃色  4  藍色 5 紫色  6 青藍色 7 白色   8灰色(字體) 白色(背景) (程式預設)*/


#if 0
WORD colorFG[]=
{
    0,
    FOREGROUND_RED,
    FOREGROUND_GREEN,
    FOREGROUND_RED | FOREGROUND_GREEN,
    FOREGROUND_BLUE,
    FOREGROUND_RED | FOREGROUND_BLUE,
    FOREGROUND_GREEN | FOREGROUND_BLUE,
    FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
    FOREGROUND_INTENSITY
};
WORD colorBG[]=
{
    0,
    BACKGROUND_RED,
    BACKGROUND_GREEN,
    BACKGROUND_RED | BACKGROUND_GREEN,
    BACKGROUND_BLUE,
    BACKGROUND_RED | BACKGROUND_BLUE,
    BACKGROUND_GREEN | BACKGROUND_BLUE,
    BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE,
    BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE
};
#endif

//#define DEBUG_OPP

const unsigned int MOVE_DELAY = 300000;

const int DICE_A_X = 80;
const int DICE_A_Y = 12;

const int DICE_B_X = 80;
const int DICE_B_Y = 21;

int gx=0;
int gy=0;
/*------------------------*/
int playMo[6];
int loser=0;
int musicStatus=0;
int mu=0;//紀錄音樂開關
int all=0;//玩家人數
int comp=0;//電腦數
int star=0;
int modeNO;//模式開關
int mapNO;//地圖編號
int diceA=2,diceB=3;
int lev1[29];
int lev2[29];
int gp[29];
int noRead=0;
int allMoney[6];//紀錄玩家總資產
int moneyList[6]= {0,10000,15000,30000,90000,120000};//起始總資產
int status[6];//紀錄玩家狀態序號
int site[6];//紀錄玩家位置(格數1~28)
int ox[6];
int oy[6];
int canMove[6];//紀錄玩家是否可動  1可
int mo=1;//骰子單/雙數設定紀錄
long int limTime;//限制時間
long int useTime;//遊戲執行時間
char mapList[MAP_LAST+1][50];//地圖清單
char gn[29][100];
char player[6][100];
char StatusList[7][20];//儲存讀入狀態
time_t t1,t2,t3,t4;//時間紀錄
/*------------------------*/
int mapChoose();//地圖選單
int peopeChoose();//玩家人數
int computerChoose();//電腦人數
int moneyChoose();//起始金額
int dice(int run);//骰子功能
int itd();//遊戲介紹
int level_up(int no);
char nameIn(int peo);//玩家名稱輸入(輸入值為玩家數量)
char comnane(int co, int al);//產生電腦名稱(輸入值為 電腦數量  ,玩家數量   )
void Opportunity2(int k);
void gOut();
void Opportunity1(int k);
void level();
void groundname();
void groundprize();
void textcolor(int fg,int bg);//字體顏色
//void gotoxy(int xpos,int ypos);//游標移動(輸入值為X軸(左右)值  ,Y軸(上下)值)
void winScreen(int a);//獲勝畫面
void delay(int time);//延遲(1000 =1秒)
void play() ;//遊戲進行
void addGame();//新建遊戲
void readGame();//讀取遊戲
void highScore();//最高分數
void worker();//工作人員
void show(int ch);//主選選單顯示
void shoWord(const char *path);//顯示檔案文字(輸入值為檔案路徑 範例:  (data內)資料夾//檔案(不須輸入.dat)  )
void backMusic(int a);//背景音樂(輸入值  0 為撥放音樂   1 為暫停音樂)
void Kanban(int no);//資訊看板
void StatusListIn();//狀態清單讀入
void mapListIn();//地圖清單讀入
void move_person(int no,int go);//人物移動(輸入值為第X個玩家  ,移動幾步)
void pause_game();//遊戲暫停
int save();//遊戲儲存
void saveFuction();//遊戲讀取
void playExp();//遊戲說明
void reSet();//全域變數重設
int modeChoose();//破關模式
void topShow();//遊戲上看版
void win(int a);//獲勝處裡(輸入值為第X個玩家)
void buy(int no);
void toll(int no);
void home1();
void buyUp(int no);
void moveBack(int no,int g);
void stopHow(int no);
void loserScreen();
void main_menu();

#define gotoxy(x, y) move((y), (x))

void textcolor(int fg, int bg)
{
#if 0
    int attr=0;
    HANDLE test=GetStdHandle(STD_OUTPUT_HANDLE); //取得標準輸出控制
    SetConsoleTextAttribute (test, attr | colorFG[fg] | colorBG[bg]);
#endif
}

void pause_screen(int x, int y, const char *msg)
{
  mvprintw(y, x, msg);
  getch();
}

//歡迎頁
void welcome()
{
  gy=2;
  shoWord("welcome");
  //mvprintw(gy, 0, "press any key to continue ...");
  //getch();
  pause_screen(0, gy, "press any key to continue ...");
}
/*------------------------*/
void reSet()
{
    int i;
    for(i=0; i<6; i++)
    {
        sprintf(player[i],"-");
        allMoney[i]=0;
        status[i]=1;
        site[i]=1;
        ox[i]=2;
        oy[i]=5;
        canMove[i]=1;
        playMo[i]=1;
    }
    for(i=0; i<29; i++)
    {
        lev1[i]=0;
        lev2[i]=0;
    }
    all=comp=t1=t2=t3=t4=modeNO=mapNO=limTime=useTime=noRead=0;
    mo=1;
}

void shoWord(const char *path)
{
    char txt[104],word[501];

    sprintf(txt,".//data//%s.dat",path);
    FILE *print = fopen(txt,"r");
    if (!print) 
    {
      mvprintw(1,1, "file open fail : %s", txt);
      getch();
      return;
    }
    while(!feof(print))
        if(fgets(word,500,print)!=NULL)
        {
            mvprintw(gy++, gx, "%s",word);
        }
    fclose(print);
  refresh();
}

void mapListIn()
{
    char txt[104],word[500];
    int i=1;
    sprintf(txt,".//data//map//list.dat");
    FILE *in = fopen(txt,"r");
    while(fscanf(in,"%s",&word)!=EOF)
    {
        sprintf(mapList[i],"%s",word);
        i++;
    }
    fclose(in);
}

void show(int c)
{
    char pri[50];
    clear();
    gy=2;
    shoWord("title");//印出標題(Rich Ver X.Y.Z)
    sprintf(pri,"menu//fuction%d",c);
    shoWord(pri);
    shoWord("menu//explanation");
}

#if 0
--------------------------------------------
|                 新建遊戲                 |
|                 讀取遊戲                 |
|                 破關記錄                 |
|                 開發人員                 |
|                 ->結束遊戲               |
--------------------------------------------
#endif

int print_stuff(int x, int y)
{
  mvprintw(y++, x, "--------------------------------------------");
  mvprintw(y++, x, "|             開發人員 Developer           |");
  mvprintw(y++, x, "|------------------------------------------|");
  mvprintw(y++, x, "|    建立者 Founders / 設計者 Designers    |");
  mvprintw(y++, x, "|                                          |");
  mvprintw(y++, x, "|          黃彥凱  Huang Yan-Kai           |");
  mvprintw(y++, x, "|          蔡士寬  Chai  Shi-Kuan          |");
  mvprintw(y++, x, "|          江村志  Jiang Cun-Zhu           |");
  mvprintw(y++, x, "|          曾家暉  Zeng  Gu-Hui            |");
  mvprintw(y++, x, "|          洪宜昌  Hong  Yi-Chang          |");
  mvprintw(y++, x, "--------------------------------------------");
}

void gOut()
{
    clear();
    int c=1,x=0,y=0,i,s,k;
    gotoxy(x,y);
    printw("---------------------------------------------");
    gotoxy(x,y+1);
    printw("|                                           |");
    gotoxy(x,y+2);
    printw("|                                           |");
    gotoxy(x,y+3);
    printw("|             你確定要離開嗎??              |");
    gotoxy(x,y+4);
    printw("|                                           |");
    gotoxy(x,y+5);
    printw("|                                           |");
    gotoxy(x,y+6);
    printw("|        ->離開           取消              |");
    gotoxy(x,y+7);
    printw("|                                           |");
    gotoxy(x,y+8);
    printw("|                                           |");
    gotoxy(x,y+9);
    printw("|                                           |");
    gotoxy(x,y+10);
    printw("---------------------------------------------");
    while(1)
    {
        k=getch();
        switch(k)
        {
        case RIGHT:
            c++;
            break;
        case LEFT:
            c--;
            break;
        case ENTER:
            if(c==2)
            {
                textcolor(0,8);
                main_menu();
                return;
            }
            if(c==1)
            {
                    clear();
                    int x=2, y=2;
                    print_stuff(x, y);
                    halfdelay(10);
                    int key=-1;
                for (int i=5 ; i > 0; --i)
                {
                    mvprintw(y+11, x, "%d 秒後自動關閉或按任意鍵離開...\n", i);
                    refresh();
                    //if (_kbhit())if(getch()!=0)break;
                    if (getch() != ERR) 
                      goto end;
                    //sleep(1);
                }
                goto end;
            }
            break;
        }
        if(c==3)c=1;
        if(c==0)c=2;
        if(c==1)
        {
            gotoxy(x,y+6);
            printw("|        ->離開           取消              |");
        }
        if(c==2)
        {
            gotoxy(x,y+6);
            printw("|          離開         ->取消              |");
        }
    }
end:
  // very ugly, fixed me
  endwin();
  exit(0);
}

void backMusic(int a)
{
#if 0
    char put[100];
    sprintf(put,"open .//data//sound//background.mp3 alias mysong");
    mciSendString(put, NULL, 0,NULL);
    musicStatus=a;
    if(a==0)mciSendString(TEXT("play MySong repeat"), NULL, 0, NULL);
    else if(a==1)mciSendString(TEXT("pause MySong"), NULL, 0, NULL);
#endif
}

void StatusListIn()
{
    char txt[104],word[500];
    int i=0;
    
    sprintf(txt,".//data//status//list%d.dat",mapNO);
    FILE *in = fopen(txt,"r");
    while(fscanf(in,"%s",&word)!=EOF)
    {
        sprintf(StatusList[i],"%s",word);
        i++;
    }
    fclose(in);
}

void groundprize()
{
    char txt[104];
    int i=0,word=0;
    sprintf(txt,".//data//play//ground%d.dat",mapNO);

    //mvprintw(0, 15, "mapNO: %d", mapNO);

    FILE *in = fopen(txt,"r");
    for(i=0; i<29; i++)
        fscanf(in,"%d",&gp[i]);
    fclose(in);
    //refresh();
    //sleep(10);
}

void groundname()
{
    char txt[104],word[500];
    int i=0;
    sprintf(txt,".//data//play//groundn%d.dat",mapNO);
    FILE *in = fopen(txt,"r");
    for(i=0; i<29; i++)
        fscanf(in,"%s",&gn[i]);
    fclose(in);
}

void moveSet()
{
    srand((unsigned)time(0));
    int x=2,y=3,n=1,k,g;
    for(k=1; k<=all+comp; k++)
    {
        g=(rand()%28)+1;
        x=ox[k];
        y=oy[k];
        n=site[k];
    #if 0
        while(g>0)
        {
            gotoxy(x+2*(k-1),y);
            printf(" ");
            if(n>0&& n<10)
                x+=12;
            else if(n>9&& n<15)
                y+=6;
            else if(n>14&& n<24)
                x=x-12;
            else if(n>23&& n<29)
                y=y-6;
            gotoxy(x+2*(k-1),y);
            textcolor(k,8);
            printf("●");
            textcolor(0,8);
            g--; n++;
            if(n==29)n=1;
            if (_kbhit())if(getch()==END)break;
        }
        site[k]=n;
        ox[k]=x;
        oy[k]=y;
        #endif
    }
}

void level()
{
    int x=1,y=7,n=1,g,i;
    for(g=28; g>0; g--)
    {
        if(n==4||n==12 ||n==19 ||n==26||n==8||n==15||(mapNO==1&&n==24))
            goto NEX;

        move(y-1, x);
        printw("          ");
        for(i=1; i<=lev1[n]; i++)
        {
            move(y-1, x+2*i-2);
            //textcolor(lev2[n],8);
          char person[5];
          sprintf(person, "%d", lev2[n]);
            printw(person);
            //printw("◆");
            //textcolor(0,8);


        }

        if(lev1[n]==0)
        {
            move(y-4, x);
            printw("          ");
            move(y-4, x);
            //textcolor(8,8);
            printw("購買$%d",gp[n]*(-1));
            //printw("購買$%d",gp[1]);
            //refresh();
            //sleep(6);
            //textcolor(0,8);
        }

        else
        {
            move(y-4, x);
            printw("          ");
            move(y-4, x);
            //textcolor(lev2[n],8);
            printw("過路$%d",gp[n]*lev1[n]/3*(-1));
            //refresh();
            //sleep(6);
            //textcolor(0,8);
        }
NEX:
        if(n>0&& n<10)x+=11;
        else if(n>9&& n<15)y+=6;
        else if(n>14&& n<24)x=x-11;
        else if(n>23&& n<29)y=y-6;
        n++;
    }
  refresh();
}


void playExp()
{
    int x=25,y=18;
    gotoxy(x,y);
    printw("---------------------------------------");
    gotoxy(x,y+1);
    printw("|          遊 戲 控 制 說 明          |");
    gotoxy(x,y+2);
    printw("---------------------------------------");
    gotoxy(x,y+3);
    printw("|   按鍵   |         說    明         |");
    gotoxy(x,y+4);
    printw("---------------------------------------");
    gotoxy(x,y+5);
    printw("|   F11    |       開啟遊戲說明       |");
    gotoxy(x,y+6);
    printw("|   F12    |       開啟/暫停音樂      |");
    gotoxy(x,y+7);
    printw("|   F8     |         暫停遊戲         |");
    gotoxy(x,y+8);
    printw("|   F7     |    開啟/關閉存檔視窗     |");
    gotoxy(x,y+9);
    printw("|   HOME   |   結束遊戲回到主選單     |");
    gotoxy(x,y+10);
    printw("---------------------------------------");
}

void topShow()
{
    t2 = time(NULL);
    useTime=t2-t1;
    gotoxy(5,0);
    char word[1][100];
    if(musicStatus==0)//輸入值  0 為撥放音樂   1 為暫停音樂
        sprintf(word[0],"開");
    if(musicStatus==1)
        sprintf(word[0],"關");
    if(modeNO==1)
    {
        printw("  <限時模式>       地圖: %s          遊戲時間:  %03ld分 %02ld秒              剩餘時間:  %03ld分 %02ld秒  背景音樂: %s",mapList[mapNO],useTime/60,useTime%60,(limTime*60-useTime)/60,((limTime*60)-useTime)%60,word[0]);
    }
    if(modeNO==2)
    {
        printw("  <清場模式>       地圖: %s          遊戲時間:  %03ld分 %02ld秒   背景音樂: %s",mapList[mapNO],useTime/60,useTime%60,word[0]);
    }
}

void delay(int time)
{
    clock_t now=clock();
    while(clock()-now<time);
}

void draw_dice(int x, int y, int point)
{
  int i=0;
  mvprintw(y, x,     "╭──────────────╮");
  for (i=0 ; i < 7 ; ++i)
    mvprintw(y+i+1, x, "│              │");
  mvprintw(y+i+1, x, "╰──────────────╯");
#if 0
  mvprintw(y, x, "│  ●      ●    │");
  mvprintw(y, x, "│              │");
  mvprintw(y, x, "│  ●      ●    │");
  mvprintw(y, x, "│              │");
  mvprintw(y, x, "│  ●      ●    │");
  mvprintw(y, x, "│              │");
#endif

  switch(point)
  {
    case 1:
    {
      mvprintw(y+4, x+7, "●");
      break;
    }
    case 2:
    {
      mvprintw(y+3, x+7, "●");
      mvprintw(y+5, x+7, "●");
      break;
    }
    case 3:
    {
      mvprintw(y+2, x+3, "●");
      mvprintw(y+4, x+7, "●");
      mvprintw(y+6, x+11, "●");
      break;
    }
    case 4:
    {
      y+=2;
      for (int i=0 ; i < 2 ; ++i)
        mvprintw(y+i*3, x+4, "●     ●");
      break;
    }
    case 5:
    {
      mvprintw(y+4, x+7, "●");
      y+=2;
      for (int i=0 ; i < 2 ; ++i)
        mvprintw(y+i*4, x+4, "●     ●");
      break;
    }
    case 6:
    {
      y+=2;
      for (int i=0 ; i < 3 ; ++i)
        mvprintw(y+i*2, x+4, "●     ●");
      break;
    }
  }
}

void clear_dice(int x, int y)
{
  int i=0;
  mvprintw(y, x,     "                ");
  for (i=0 ; i < 7 ; ++i)
    mvprintw(y+i+1, x, "                ");
  mvprintw(y+i+1, x, "                ");
}

int dice(int run)
{
#ifdef DEBUG_OPP
  //return 23;
  static int bb=0;

  if (bb<5)
  {
    ++bb;
    return 3;
  }
  else
  {
    ++bb;
    return 0;
  }
#endif

  if(run==0)
  {
    return 0;
  }
  //return 1;
    int a=1,b=0,x=80,y=11,i,j=0;
    char sha[10],shb[10];
    srand((unsigned)time(0));
    for (int i=0 ; i < 15; ++i)
    {
        if(run ==1)
        {
            diceA=(rand()%6)+1;
            if(mo==1)diceB=(rand()%6)+1;
        }

        draw_dice(x, y+1, diceA);

        if(mo==1)
          draw_dice(x, y+10, diceB);
        else
          clear_dice(x, y+10);

        topShow();
        refresh();
        usleep(100000);
    }
  if(mo==0)return diceA;
  return diceB+diceA;
}

void Kanban(int no)
{
    int x=25,y=9,i,j;
    gotoxy(x,y);
    printw("---------------------------------------");
    gotoxy(x,y+1);
    printw("|        玩  家  狀  態  看  版       |");
    gotoxy(x,y+2);
    printw("---------------------------------------");
    gotoxy(x,y+3);
    printw("|顏  色|  名  稱  | 總 資 產 | 狀  態 |");
    gotoxy(x,y+4);
    printw("---------------------------------------");
    for(i=0; i<all+comp+1; i++)
    {
        gotoxy(x,y+5+i);
        printw("|      |          |          |        |");
        //textcolor(i+1,8);
        gotoxy(x+2,y+5+i);

        char person[5];

        if(i+1==no) 
        {
          sprintf(person, "->%d", i+1);
          //printw("->●");
        }
        else 
        {
          sprintf(person, "  %d", i+1);
          //printw("●");
        }
        printw(person);

        //textcolor(0,8);
    }
    gotoxy(x,y+4+i);
    printw("---------------------------------------");
    for(j=1; j<=all+comp; j++)
    {
        gotoxy(x+8,y+4+j);
        printw("%s",player[j]);
    }
    for(j=1; j<=all+comp; j++)
    {
        gotoxy(x+19,y+4+j);
        printw("%d",allMoney[j]);
    }
    for(j=1; j<=all+comp; j++)
    {
        gotoxy(x+30,y+4+j);
        printw("%s",StatusList[status[j]]);
    }
}

void win(int a)
{
    int max=0,i,maxNO=1,j=0,k,l;
    if(allMoney[a]<0)
    {
        allMoney[a]=0;
        site[a]=0;
        loser--;
        for(k=1; k<=28; k++)
        {
            if(lev2[k]==a)
            {
                lev1[k]=0;
                lev2[k]=0;
            }
        }
    }

    for(l=1; l<=all+comp; l++)
    {
        if(allMoney[l]==0)
            j++;
    }
    if(loser==0 )loserScreen();
    if( ((modeNO==1&&useTime>limTime*60) || (j>=all+comp-1) )&&loser!=0 )
    {
        max=allMoney[1];
#if 0
        // orginal code
        SYSTEMTIME sys;
        GetLocalTime( &sys );
#endif
        time_t cur_time = time(NULL);
        struct tm * tm = localtime(&cur_time);

        for(i=1; i<all+comp+1; i++)
        {
            if(max<allMoney[i])
            {
                max=allMoney[i];
                maxNO=i;
            }
        }
        FILE *out = fopen(".//data//win//Value.dat","a");
        fprintf(out,"%d\n",1);
        fprintf(out,"%s\n",player[maxNO]);
        fprintf(out,"%4d/%02d/%02d_%02d:%02d\n",tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min);
        fprintf(out,"%s\n",mapList[mapNO]);
        fclose(out);
        winScreen(maxNO);
    }
    //return 0;
}

void stopHow(int no)
{
    //textcolor(5,8);
    int x=48,y=18;
    gotoxy(x-2,y-1);
    printw("-------------------------------------------");
    gotoxy(x-2,y);
    printw("|                                         |");
    gotoxy(x-2,y+1);
    printw("-------------------------------------------");
    gotoxy(x,y);
    printw("%s  %s 無法前進, 還有%d次",player[no],StatusList[status[no]],canMove[no]-2);
    delay(2000);
    //textcolor(0,8);
    gotoxy(x-2,y-1);
    printw("                                           ");
    gotoxy(x-2,y);
    printw("                                           ");
    gotoxy(x-2,y+1);
    printw("                                           ");
}

void toll(int no)
{
    textcolor(1,8);
    int q,a,x=48,y=18;
    gotoxy(x-2,y-1);
    printw("-------------------------------------------");
    gotoxy(x-2,y);
    printw("|                                         |");
    gotoxy(x-2,y+1);
    printw("-------------------------------------------");
    gotoxy(x,y);
    if(canMove[lev2[site[no]]]>1)
        printw("所有者%s %s，免付過路費~~",player[lev2[site[no]]],StatusList[status[lev2[site[no]]]]);
    else
    {
        printw("此處為%s所有，需支付過路費$%d",player[lev2[site[no]]],gp[site[no]]*lev1[site[no]]/3*-1);
        allMoney[no]=allMoney[no]+gp[site[no]]*lev1[site[no]]/3;
        allMoney[lev2[site[no]]]=allMoney[lev2[site[no]]]-gp[site[no]]*lev1[site[no]]/3;
    }
    //delay(1500);
  refresh();
    sleep(3);
    textcolor(0,8);
    gotoxy(x-2,y-1);
    printw("                                           ");
    gotoxy(x-2,y);
    printw("                                           ");
    gotoxy(x-2,y+1);
    printw("                                           ");
  refresh();
}

int lev(int no)
{
    if(lev1[site[no]]>0&&lev2[site[no]]!=no)
      toll(no);
    textcolor(0,8);
    srand((unsigned)time(0));
    int a,i,c=1,x=37,y=15,k;
    if(lev1[site[no]]==0)
    {
        c=1;
        gotoxy(x,y);
        printw("----------------------------------------------------");
        gotoxy(x,y+1);
        printw("|                                                  |");
        gotoxy(x,y+2);
        printw("|                   請問是否購買??                 |");
        gotoxy(x,y+3);
        printw("|                                                  |");
        gotoxy(x,y+4);
        printw("|           名稱：                                 |");
        gotoxy(x,y+5);
        printw("|           價格：                                 |");
        gotoxy(x,y+6);
        printw("|                                                  |");
        gotoxy(x,y+7);
        printw("|                                                  |");
        gotoxy(x,y+8);
        printw("|              ->是                   否           |");
        gotoxy(x,y+9);
        printw("|                                                  |");
        gotoxy(x,y+10);
        printw("----------------------------------------------------");
        gotoxy(x+25,y+4);
        printw("%s",gn[site[no]]);
        gotoxy(x+23,y+5);
        printw("%d",-1*gp[site[no]]);
        refresh();
        while(1)
        {
            if(all<no)
            {
                c=rand() % 2+1;
                goto BGC;
            }
            k=getch();
            switch(k)
            {
            case RIGHT:
                c++;
                break;
            case LEFT:
                c--;
                break;
            case ENTER:
BGC2:
                if(c==1)
                {
                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    buy(no);
                    textcolor(0,8);
                    refresh();
                    return 0;
                }
                if(c==2)
                {
                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    textcolor(0,8);
                    refresh();
                    return 0;
                }
                break;
            }
            if(c==3) c=1;
            if(c==0) c=2;
BGC:
            if(c==1)
            {
                gotoxy(x,y+8);
                printw("|              ->是                   否           |");
            }
            if(c==2)
            {
                gotoxy(x,y+8);
                printw("|                是                 ->否           |");
            }

            if(all<no)
            {
              refresh();
                //usleep(400000);
                sleep(1);
                goto BGC2;
            }
        }
    }
    if(lev1[site[no]]==5)goto FU;
    if(no==lev2[site[no]]&&lev1[site[no]]<6)
    {
        c=1;
        gotoxy(x,y);
        printw("----------------------------------------------------");
        gotoxy(x,y+1);
        printw("|                                                  |");
        gotoxy(x,y+2);
        printw("|                  請選擇升級或賣出                |");
        gotoxy(x,y+3);
        printw("|                                                  |");
        gotoxy(x,y+4);
        printw("|        ->升級          賣出        都不要        |");
        gotoxy(x,y+5);
        printw("|                                                  |");
        gotoxy(x,y+6);
        printw("----------------------------------------------------");
        while(1)
        {
            if(all<no)
            {
                c=rand() % 3+1;
                goto BGC3;
            }
            k=getch();
            switch(k)
            {
            case RIGHT:
                c++;
                break;
            case LEFT:
                c--;
                break;
            case ENTER:
BGC4:
                if(c==1)
                {
                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    goto UP1;
                    refresh();
                    return 0;
                }
                if(c==2)
                {
                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    goto UP2;
                    refresh();
                    return 0;
                }
                if(c==3)
                {
                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    refresh();
                    return 0;
                }
                break;
            }
            if(c==4) c=1;
            if(c==0) c=3;
BGC3:
            if(c==1)
            {
                gotoxy(x,y+4);
                printw("|        ->升級          賣出        都不要        |");
            }
            if(c==2)
            {
                gotoxy(x,y+4);
                printw("|          升級        ->賣出        都不要        |");
            }
            if(c==3)
            {
                gotoxy(x,y+4);
                printw("|          升級          賣出      ->都不要        |");
            }
            if(all<no)
            {
                delay(850);
                goto BGC4;
            }
        }
    }

UP1:
    if(no==lev2[site[no]])
    {
        c=1;
        gotoxy(x,y);
        printw("----------------------------------------------------");
        gotoxy(x,y+1);
        printw("|                                                  |");
        gotoxy(x,y+2);
        printw("|                   請問是否升級??                 |");
        gotoxy(x,y+3);
        printw("|                                                  |");
        gotoxy(x,y+4);
        printw("|             名稱：                               |");
        gotoxy(x,y+5);
        printw("|   過路費  升級前：                               |");
        gotoxy(x,y+6);
        printw("|           升級後：                               |");
        gotoxy(x,y+7);
        printw("|             等級:                                |");
        gotoxy(x,y+8);
        printw("|         升級費用：                               |");
        gotoxy(x,y+9);
        printw("|                                                  |");
        gotoxy(x,y+10);
        printw("|              ->是                   否           |");
        gotoxy(x,y+11);
        printw("|                                                  |");
        gotoxy(x,y+12);
        printw("----------------------------------------------------");
        gotoxy(x+25,y+4);
        printw("%s",gn[site[no]]);
        gotoxy(x+25,y+5);
        printw("%d",-1*gp[site[no]]*lev1[site[no]]/3);
        gotoxy(x+25,y+6);
        printw("%d",-1*gp[site[no]]*(lev1[site[no]]+1)/3);
        gotoxy(x+23,y+7);
        printw("%d",lev1[site[no]]);
        gotoxy(x+25,y+8);
        printw("%d",-1*gp[site[no]]/4*lev1[site[no]]);
        while(1)
        {
            if(all<no)
            {
                c=rand() % 2+1;
                goto BGC5;
            }
            k=getch();
            switch(k)
            {
            case RIGHT:
                c++;
                break;
            case LEFT:
                c--;
                break;
            case ENTER:
BGC6:
                if(c==1)
                {
                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    buyUp(no);
                    textcolor(0,8);
                    refresh();
                    return 0;
                }
                if(c==2)
                {
                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    textcolor(0,8);
                    refresh();
                    return 0;
                }
                break;
            }
            if(c==3) c=1;
            if(c==0) c=2;
BGC5:
            if(c==1)
            {
                gotoxy(x,y+10);
                printw("|              ->是                   否           |");
            }
            if(c==2)
            {
                gotoxy(x,y+10);
                printw("|                是                 ->否           |");
            }
            if(all<no)
            {
                delay(850);
                goto BGC6;
            }
        }
    }

UP2:
    if(no==lev2[site[no]])
    {
        c=1;
        gotoxy(x,y);
        printw("----------------------------------------------------");
        gotoxy(x,y+1);
        printw("|                                                  |");
        gotoxy(x,y+2);
        printw("|                       賣出確認                   |");
        gotoxy(x,y+3);
        printw("|                                                  |");
        gotoxy(x,y+4);
        printw("|             名稱：                               |");
        gotoxy(x,y+5);
        printw("|             等級:                                |");
        gotoxy(x,y+6);
        printw("|             獲利：                               |");
        gotoxy(x,y+7);
        printw("|                                                  |");
        gotoxy(x,y+8);
        printw("|              ->是                   否           |");
        gotoxy(x,y+9);
        printw("|                                                  |");
        gotoxy(x,y+10);
        printw("----------------------------------------------------");
        gotoxy(x+25,y+4);
        printw("%s",gn[site[no]]);
        gotoxy(x+23,y+5);
        printw("%d",lev1[site[no]]);
        gotoxy(x+25,y+6);
        printw("%d",-1*gp[site[no]]*15/10);
        while(1)
        {
            if(all<no)
            {
                c=rand() % 2+1;
                goto BGC7;
            }
            k=getch();
            switch(k)
            {
            case RIGHT:
                c++;
                break;
            case LEFT:
                c--;
                break;
            case ENTER:
BGC8:
                if(c==1)
                {
                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    allMoney[no]=allMoney[no]-gp[site[no]]*15/10;
                    lev1[site[no]]=0;
                    lev2[site[no]]=0;
                    textcolor(0,8);
                    refresh();
                    return 0;
                }
                if(c==2)
                {
                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    textcolor(0,8);
                    refresh();
                    return 0;
                }
                break;
            }
            if(c==3) c=1;
            if(c==0) c=2;
BGC7:
            if(c==1)
            {
                gotoxy(x,y+8);
                printw("|              ->是                   否           |");
            }
            if(c==2)
            {
                gotoxy(x,y+8);
                printw("|                是                 ->否           |");
            }
            if(all<no)
            {
                delay(850);
                goto BGC8;
            }
        }
    }
FU:
    if(no==lev2[site[no]]&&lev1[site[no]]==5)
    {
        c=2;
        gotoxy(x,y);
        printw("----------------------------------------------------");
        gotoxy(x,y+1);
        printw("|                                                  |");
        gotoxy(x,y+2);
        printw("|          等級已達上限可選擇賣出或取消            |");
        gotoxy(x,y+3);
        printw("|                                                  |");
        gotoxy(x,y+4);
        printw("|        ->賣出                    取消            |");
        gotoxy(x,y+5);
        printw("|                                                  |");
        gotoxy(x,y+6);
        printw("----------------------------------------------------");
        while(1)
        {
            if(all<no)
            {
                c=rand() % 3+2;
                goto BGC9;
            }
            k=getch();
            switch(k)
            {
            case RIGHT:
                c++;
                break;
            case LEFT:
                c--;
                break;
            case ENTER:
BGC10:
                if(c==2)
                {

                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    goto UP2;
                    refresh();
                    return 0;
                }
                if(c==3)
                {
                    for(i=0; i<=13; i++)
                    {
                        gotoxy(x,y+i);
                        printw("                                                        ");
                    }
                    refresh();
                    return 0;
                }
                break;
            }
            if(c==4) c=2;
            if(c==1) c=3;
BGC9:
            if(c==2)
            {
                gotoxy(x,y+4);
                printw("|        ->賣出                    取消            |");
            }
            if(c==3)
            {
                gotoxy(x,y+4);
                printw("|          賣出                  ->取消            |");
            }
            refresh();
            if(all<no)
            {
                usleep(200000);
                goto BGC10;
            }
        }
    }
}

// for map1
void Opportunity1(int k)
{
    textcolor(5,8);
    int q,x=48,y=18;
    srand((unsigned)time(0));
#ifdef DEBUG_OPP
    static int a=0;
#else
    int a=(rand()%10)+1;
#endif
    //gotoxy(0,42);scanf("%d",&a);
BAC:
    gotoxy(x-2,y-1);
    printw("-------------------------------------------");
    gotoxy(x-2,y);
    printw("|                                         |");
    gotoxy(x-2,y+1);
    printw("-------------------------------------------");
    switch(a)
    {
    case 0:
        q=rand()%5+1;
        gotoxy(x,y);
        printw("便當被搶，獲賠償金 %d 元",q*10);
        allMoney[k]=allMoney[k]+q*10;
        break;
    case 1:
        gotoxy(x,y);
        q=rand()%5+1;
        printw("中午不吃，省下%d元",q*10);
        allMoney[k]=allMoney[k]+q*10;
        break;
    case 2:
        gotoxy(x,y);
        q=rand()%5+1;
        printw("遭藍鵲攻擊，醫藥費%d元",q*10);
        allMoney[k]=allMoney[k]-q*10;
        break;
    case 3:
        gotoxy(x,y);
        q=rand()%5+1;
        printw("告白被打槍，崩潰原地停留%d回合",q);
        canMove[k]=canMove[k]+q;
        status[k]=6;
        break;
    case 4:
        gotoxy(x,y);
        q=rand()%5+1;
        printw("12年國教賠償%d元",q*15);
        allMoney[k]=allMoney[k]+q*15;
        break;
    case 5:
        gotoxy(x,y);
        printw("上課睡覺，總資產減少10%%");
        allMoney[k]=allMoney[k]*0.9;
        break;
    case 6:
        gotoxy(x,y);
        q=rand()%5+1;
        printw("英文被當， 重修費用%d元",q*12);
        allMoney[k]=allMoney[k]-q*12;
        break;
    case 7:
        gotoxy(x,y);
        q=rand()%5+1;
        printw("服儀不整 吊銷%d元",q*18);
        allMoney[k]=allMoney[k]-q*18;
        break;
    case 8:
        q=rand()%5+1;
        gotoxy(x,y);
        printw("偷了一條麵包，罰站%d回合",q);
        canMove[k]=canMove[k]+q;
        status[k]=5;
        break;
    case 9:
        q=rand()%28+1;
        //q=8;
        //gotoxy(0,42);scanf("%d",&q);
        gotoxy(x,y);
        printw("退後%d格",q);
        refresh();
        //delay(500);
        sleep(2);
        moveBack(k,q);

        // site[k] == 4 是什麼??
        if(site[k]==4||site[k]==8||site[k]==12||site[k]==15||site[k]==19||site[k]==24||site[k]==26)
            goto NEX;
            lev(k);
        NEX:
        if(site[k]==15)
        {
            canMove[k]=canMove[k]+3;
            status[k]=3;
        }
        if(site[k]==8)
        {
            canMove[k]=canMove[k]+1;
            status[k]=2;
        }
        if(site[k]==24)
        {
            canMove[k]=canMove[k]+1;
            status[k]=4;
        }
        if(site[k]==4||site[k]==12||site[k]==19||site[k]==26)Opportunity1(k);
        goto NEXT;
        break;
    case 10:
        gotoxy(x,y);
        q=rand()%5+1;
        printw("獲得獎學金%d元                    ",q*20);
        allMoney[k]=allMoney[k]+q*20;
        break;
    }
    refresh();
    //delay(2000);
    sleep(2);
NEXT:
#ifdef DEBUG_OPP
    a=((a+1)%11);
#endif
    textcolor(0,8);
    gotoxy(x-2,y-1);
    printw("                                           ");
    gotoxy(x-2,y);
    printw("                                           ");
    gotoxy(x-2,y+1);
    printw("                                           ");
    refresh();
}

// for map2
void Opportunity2(int k)
{
    textcolor(5,8);
    int q,x=48,y=18;
    srand((unsigned)time(0));
#ifdef DEBUG_OPP
    static int a=1;
#else
    int a=(rand()%4)+1;
#endif
BAC:
    gotoxy(x-2,y-1);
    printw("-------------------------------------------");
    gotoxy(x-2,y);
    printw("|                                         |");
    gotoxy(x-2,y+1);
    printw("-------------------------------------------");
    switch(a)
    {
    case 1:
        q=rand()%8+1;
        gotoxy(x,y);
        printw("退後%d格",q);
        //delay(500);
        refresh();
        sleep(2);
        moveBack(k,q);
        if(mapNO==2&&(site[k]==4||site[k]==8||site[k]==12||site[k]==15||site[k]==19||site[k]==26))
          goto JUMP;
        lev(k);
JUMP:
        if(site[k]==15)
        {
            canMove[k]=canMove[k]+3;
            status[k]=3;
        }
        if(site[k]==8)
        {
            canMove[k]=canMove[k]+1;
            status[k]=2;
        }
        if(site[k]==4||site[k]==12||site[k]==19||site[k]==26)Opportunity2(k);
        goto NEXT;
        break;
    case 2:
        q=rand()%24+12;
        gotoxy(x,y);
        printw("獲得旅遊獎金$ %d",q*100);
        allMoney[k]=allMoney[k]+q*100;
        break;
    case 3:
        q=rand()%8+5;
        gotoxy(x,y);
        printw("行李超重加收運費$ %d",q*100);
        allMoney[k]=allMoney[k]-q*30;
        break;
    case 4:
        q=rand()%12+8;
        gotoxy(x,y);
        printw("遇到扒手,損失$ %d",q*100);
        allMoney[k]=allMoney[k]-q*80;
        break;
    }
    refresh();
    sleep(2);
NEXT:
#ifdef DEBUG_OPP
    a=((a+1)%5);
#endif
    textcolor(0,8);
    gotoxy(x-2,y-1);
    printw("                                           ");
    gotoxy(x-2,y);
    printw("                                           ");
    gotoxy(x-2,y+1);
    printw("                                           ");
    refresh();
}

void buy(int no)
{
    lev1[site[no]]++;
    lev2[site[no]]=no;
    allMoney[no]=allMoney[no]+gp[site[no]];
}
/*------------------------*/
void buyUp(int no)
{
    lev1[site[no]]++;
    lev2[site[no]]=no;
    allMoney[no]=allMoney[no]+gp[site[no]]/4*lev1[site[no]];
}

void moveBack(int no,int g)
{
    int x=2,y=3,n=1;
    x=ox[no];
    y=oy[no];
    n=site[no];
    while(g>0)
    {
        topShow();
        gotoxy(x+2*(no-1),y);
        printw(" ");
        if(n==1 || n>24&&n<29)
            y+=6;
        else if(n>15 &&n<25)
            x+=11;
        else if(n>10&&n<16)
            y=y-6;
        else if(n>1&&n<11)
            x=x-11;
        gotoxy(x+2*(no-1),y);
        textcolor(no,8);
        //printw("●");

        char person[5];
        sprintf(person, "%d", no);
        printw(person);

        textcolor(0,8);
        g--;n--;
        if(n==0) 
          n=28;
      refresh();
        usleep(MOVE_DELAY);
    }
    site[no]=n;
    ox[no]=x;
    oy[no]=y;
}

void move_person(int no,int g)
{
    int x=2,y=3,n=1;
    x=ox[no];
    y=oy[no];
    n=site[no];
    while(g>0)
    {
       topShow();
       gotoxy(x+2*(no-1),y);
        printw(" ");
        if(n>0&& n<10)
            x+=11;
        else if(n>9&& n<15)
            y+=6;
        else if(n>14&& n<24)
            x=x-11;
        else if(n>23&& n<29)
            y=y-6;
        gotoxy(x+2*(no-1),y);
        textcolor(no,8);
        char person[5];
        sprintf(person, "%d", no);
        //printw("●");
        printw(person);
        textcolor(0,8);
        refresh();
        g--; n++;
        if(n==29) n=1;

        //if (_kbhit()) if(getch()==END) break;
        usleep(MOVE_DELAY);
    }
    site[no]=n;
    ox[no]=x;
    oy[no]=y;
}

void play()
{
    //system("mode con cols=123 lines=42");//視窗大小
    int go,c,j,i,no=1,comTure=0,k=0,key,run=0,l=0;
    srand((unsigned)time(0));
    char mapin[50];
    clear();
    mvprintw(3, 0, "Loading...");
    refresh();
    //getch();

    StatusListIn();
    groundprize();
    groundname();
    clear();
    t1 = time(NULL);
    //printf("\n\n");
    sprintf(mapin,"play//map%d",mapNO);
    gy=1;
    shoWord(mapin); // draw map
    if(noRead==0)
    {
    //exit(-1);
    moveSet();
    loser=all;
    }

    for(i=0; i<all+comp; i++)//印起始棋子
    {
        //gotoxy(ox[i+1]+i*2,oy[i+1]);
        //textcolor(i+1,8);//顏色變換
        //attron(COLOR_PAIR(i));
        char person[5];
        sprintf(person, "%d", i+1);
        mvprintw(oy[i+1], ox[i+1]+i*2, person);
        //mvprintw(oy[i+1], ox[i+1]+i*2, "●");
        //attroff(COLOR_PAIR(i));
        //textcolor(0,8);//顏色還原
    }
    //attron(A_NORMAL);
NE:
    level();
    playExp();
    //dice(0);
    mvprintw(10, 80, "按ENTER 擲骰子");
    mvprintw(11, 80, "按END切換單/雙骰子");
    draw_dice(DICE_A_X, DICE_A_Y, 1);
    draw_dice(DICE_B_X, DICE_B_Y, 2);
    while(1)//遊戲迴圈
    {
        topShow();
        Kanban(no);
        comTure=0;
        if(canMove[no]!=1)
        {
            stopHow(no);
            canMove[no]--;
            if(canMove[no]==1)
                status[no]=1;
            goto OVER;
        }
        if(site[no]==0)
        {
            gotoxy(ox[no]+2*(no-1),oy[no]);
            printf(" ");
            status[no]=0;
            goto OVER;
        }
        run=0;

        if(all<no)
        {
            mo=rand()%2+1;
            mo--;
            go=dice(1);
            goto NEXT;
        }
        while(1)
        {
            mo=playMo[no];
            //dice(0);
            key=getch();
            switch(key)
            {
            case ENTER:
                if(canMove[no]==1)go=dice(1);
                goto NEXT;
                break;
            case END:
                if(mo==0)
                    playMo[no]=mo=1;
                else
                    playMo[no]=mo=0;
                break;
            case F8:
                pause_game();
                goto NE;
                break;
            case F7:
                save();
                goto NE;
                break;
            case HOME:
                home1();
                goto NE;
                break;
            case F12:
                if(mu==0)
                {
                    backMusic(1);
                    mu++;
                }
                else
                {
                    backMusic(0);
                    mu--;
                }
                break;
            case F11:
                itd();
                goto NE;
                break;
            case F9:
                gotoxy(10,39);
                scanf("%d",&go);
                goto TEX;
                break;
            }
            topShow();
            //dice(0);
        }
NEXT:
TEX:
        move_person(no,go);
        level();
        if(mapNO==1)
        {
            if(site[no]==15)
            {
                canMove[no]=canMove[no]+3;
                status[no]=3;
                goto FUC;
            }
            if(site[no]==8)
            {
                canMove[no]=canMove[no]+1;
                status[no]=2;
                goto FUC;
            }
            if(site[no]==24)
            {
                canMove[no]=canMove[no]+1;
                status[no]=4;
                goto FUC;
            }
            if(site[no]==4) // 命運
            {
                Opportunity1(no);
                goto FUC;
            }
            if(site[no]==12) // 機會
            {
                Opportunity1(no);
                goto FUC;
            }
            if(site[no]==19) // 命運
            {
                Opportunity1(no);
                goto FUC;
            }
            if(site[no]==26) // 機會
            {
                Opportunity1(no);
                goto FUC;
            }
        }
        if(mapNO==2)
        {
            if(site[no]==15)
            {
                canMove[no]=canMove[no]+3;
                status[no]=3;
                goto FUC;
            }
            if(site[no]==8)
            {
                canMove[no]=canMove[no]+1;
                status[no]=2;
                goto FUC;
            }
            if(site[no]==4)
            {
                Opportunity2(no);
                goto FUC;
            }
            if(site[no]==12)
            {
                Opportunity2(no);
                goto FUC;
            }
            if(site[no]==19)
            {
                Opportunity2(no);
                goto FUC;
            }
            if(site[no]==26)
            {
                Opportunity2(no);
                goto FUC;
            }
        }
        lev(no);
FUC:
        level();
        playExp();
        //dice(0);
        draw_dice(DICE_A_X, DICE_A_Y, 1);
        draw_dice(DICE_B_X, DICE_B_Y, 2);
        Kanban(no);
OVER:
        win(no);
        if(site[no]==0)
        {
            gotoxy(ox[no]+2*(no-1),oy[no]);
            printw(" ");
            status[no]=0;
        }
        no++;//下一玩家
        level();
        if(no==all+comp+1)//順序輪迴回
            no=1;//第幾個玩家
        refresh();
    }
}

void addGame()
{
    //system("cls");
    int map,peo,com,mon,f;
    char txt[50];
    all=0;
    noRead=0;
    map=mapChoose();
    peo=peopeChoose();
    com=computerChoose();
    mon=moneyChoose();
back:
    modeChoose();
    clear();
    gy=2;
    shoWord("title");
    //printf("\n");
    shoWord("addcheck");
    //printf("\n");
    sprintf(txt,"map//c%d",map);
    shoWord(txt);
    //printf("\n");
    sprintf(txt,"peope//c%d",peo);
    shoWord(txt);
    //printf("\n");
    sprintf(txt,"computer//c%d",com);
    shoWord(txt);
    //printf("\n");
    sprintf(txt,"money//c%d",mon);
    shoWord(txt);
    //printf("\n");
    sprintf(txt,"mode//c%d",modeNO);
    shoWord(txt);
    //printf("\n");
    shoWord("addend");
    //printf("\n");
    shoWord("addexp2");
    //printf("\n");
    if(modeNO==1)
    {
        //gotoxy(32,10);
        mvprintw(gy-7, 32, "%ld",limTime);
        refresh();
    }
    while(1)
    {
        f=getch();
        switch(f)
        {
        case LEFT:
            goto back;
            break;
        case HOME:
            //main();
            break;
        case RIGHT:
        case ENTER:
            play();
            break;
        case F12:
            if(mu==0)
            {
                backMusic(1);
                mu++;
            }
            else
            {
                backMusic(0);
                mu--;
            }
            break;
        }
    }
}

int mapChoose()
{
    int c=1;
    char pri[50];
    while(1)
    {
      clear();
      gy=2;
        shoWord("title");
        shoWord("map//title");
        sprintf(pri,"map//%d",c);
        shoWord(pri);
        shoWord("menu//explanation");
        switch (getch())
        {
        case UP:
            c--;
            if (c<MAP_FIRST) c=MAP_LAST;
            break;
        case DOWN:
            c++;
            if (c >MAP_LAST) c=MAP_FIRST;
            break;
        /*選擇處理*/
        case ENTER:
        case RIGHT:
            mapNO=c;
            return c;
            break;
        case LEFT:
        case HOME:
        {
            main_menu();
            break;
        }
        case F12:
            if(mu==0)
            {
                backMusic(1);
                mu++;
            }
            else
            {
                backMusic(0);
                mu--;
            }
            break;
        }
        //printf("\n");
    }
}

char nameIn(int peo)
{
    int i,j=1,c;
    char tem[100];
    clear();
    gy=2;
    shoWord("title");
    //printf("\n");
    shoWord("namein//title");
    for(i=0; i<peo; i++)
      mvprintw(gy++, 0, "|                                          |\n");
    mvprintw(gy++, 0, "--------------------------------------------\n");
    int py = gy - (peo+1);
    char play_info[255] = "|                                          |";
    for(i=1; i<=peo; i++)
    {
in:
        //gotoxy(2,5+j);
        mvprintw(py, 2, "輸入玩家%d名稱(最多十個字元):",j);
        //gotoxy(32,5+j);
        refresh();
        getnstr(tem, 50);
        if(strlen(tem)>10 ||strlen(tem)==0) goto in;
        sprintf(player[j],"%s",tem);
#if 0
        //gotoxy(2,5+j);
        mvprintw(py+j-1, 2, "                                       ");
        //gotoxy(0,5+j);
        mvprintw(py+j-1, 2, "|                                          |");
        //gotoxy(2,5+j);
#endif
        mvprintw(py, 2, "玩家 %d : %s                     ",j,player[j]);
        j++;
        ++py;
        refresh();
    }
    //gotoxy(0,5+j);
    mvprintw(py, 0, "--------------------------------------------\n");
    refresh();
    getch();
    //system("pause");
}

int peopeChoose()
{
    int c=PEOPE_FIRST,l,k;
    char pri[50];
    while(1)
    {
        //system("cls");
        gy=2;
        shoWord("title");
        //printf("\n");
        shoWord("peope//title");
        //printf("\n");
        sprintf(pri,"peope//%d",c);
        shoWord(pri);
        //printf("\n");
        shoWord("menu//explanation");
        k=getch();
        switch(k)
        {
        case UP:
            c--;
            if (c<PEOPE_FIRST) c=PEOPE_LAST;
            break;
        case DOWN:
            c++;
            if (c >PEOPE_LAST) c=PEOPE_FIRST;
            break;
        case ENTER:
        case RIGHT:  /*選擇處理*/
            all=c;
            nameIn(c);
            return c;
            break;
        case LEFT:
            mapChoose();
            break;
        case HOME :
            //main ();
            break;
        case F12:
            if(mu==0)
            {
                backMusic(1);
                mu++;
            }
            else
            {
                backMusic(0);
                mu--;
            }
            break;
        }
    }
}

char comnane(int co, int al)
{
    char name[NAMELIST+1][50],txt[50];
    int i,j,k=al+1,a[5-al],m=0;
    sprintf(txt,".//data//comname//list.dat");
    FILE *in = fopen(txt,"r");
    while(!feof(in))
        for(i=1; i<NAMELIST+1; i++)
            fscanf(in,"%s",name[i]);
    fclose(in);
    srand((unsigned)time(NULL));
    clear();
    gy=2;
    shoWord("title");
    //printf("\n");
    shoWord("comname//title");
    for(i=0; i<co; i++)
        mvprintw(gy++, 0, "|                                          |\n");
    mvprintw(gy, 0, "--------------------------------------------\n");
    gy-=co;
    for(i=1; i<=co; i++)
    {
re:
        j=(rand()%NAMELIST)+1;
        for(m=1; m<i+1; m++)
            if(j==a[m]) goto re;
        a[i]=j;
        sprintf(player[k],"%s",name[j]);
        //gotoxy(2,5+i);
        mvprintw(gy++, 2, "電腦 %d : %s",i,player[k]);
        k++;j++;
    }
    mvprintw(gy, 0, "--------------------------------------------\n");
    //system("pause");
  getch();
}
/*------------------------*/
int computerChoose()
{
    int c,las=0,firs=0,k;
    char pri[50];
    c=firs;
    if(all==5) return 0;
    if(all==1) firs=1;
    c=firs;
      while(1)
    {
        las=COMPUTER_LAST-all+1;
        firs=COMPUTER_FIRST;
        if(all==1) firs=1;
        clear();
        gy=2;
        shoWord("title");
        //printf("\n");
        shoWord("computer//title");
        //printf("\n");
        sprintf(pri,"computer//%d",c);
        shoWord(pri);
        //printf("\n");
        shoWord("menu//explanation");
        k=getch();
        switch(k)
        {
        case UP:
            c--;
            if (c<firs) c=las;
            break;
        case DOWN:
            c++;
            if (c >las) c=firs;
            break;
        case ENTER:
        case RIGHT:  /*選擇處理*/
            if(c!=0) comnane(c,all);
            comp=c;
            return c;
            break;
        case LEFT:
            peopeChoose();
            break;
        case HOME :
            //main ();
            break;
        case F12:
            if(mu==0)
            {
                backMusic(1);
                mu++;
            }
            else
            {
                backMusic(0);
                mu--;
            }
            break;
        }
        printf("\n");
    }
}
/*------------------------*/
int moneyChoose()
{
    int c=1,i,k;
    char pri[50];
    while(1)
    {
        clear();
        gy=2;
        shoWord("title");
        //printf("\n");
        shoWord("money//title");
        //printf("\n");
        sprintf(pri,"money//%d",c);
        shoWord(pri);
        //printf("\n");
        shoWord("menu//explanation");
        k=getch();
        switch(k)
        {
        case UP:
            c--;
            if (c<MONEY_FIRST) c=MONEY_LAST;
            break;
        case DOWN:
            c++;
            if (c >MONEY_LAST) c=MONEY_FIRST;
            break;
        case ENTER:
        case RIGHT:  /*選擇處理*/
            for(i=1; i<=all+comp; i++)
                allMoney[i]=moneyList[c];
            return c;
            break;
        case LEFT:
            if(all==5)
                peopeChoose();
            //computerChoose(all);
            break;
        case HOME :
            //main ();
            break;
        case F12:
            if(mu==0)
            {
                backMusic(1);
                mu++;
            }
            else
            {
                backMusic(0);
                mu--;
            }
            break;
        }
        printf("\n");
    }
}

void timeSet()
{
    char pri[50];
back:
    clear();
    gy=2;
    shoWord("title");
    //printf("\n");
    sprintf(pri,"mode//timein");
    shoWord(pri);
    //printf("\n");
#if 0
    gotoxy(32,6);
#endif
    move(gy-2, 32);
    scanw("%ld",&limTime);
    if(limTime<=0||limTime>999)
      goto back;
}

int modeChoose()
{
    int c=1,k;
    char pri[50];
    while(1)
    {
        clear();
        gy=2;
        shoWord("title");
        //printf("\n");
        sprintf(pri,"mode//%d",c);
        shoWord(pri);
        //printf("\n");
        shoWord("menu//explanation");
        k=getch();
        switch(k)
        {
        case UP:
            c--;
            if (c<MODE_FIRST) c=MODE_LAST;
            break;
        case DOWN:
            c++;
            if (c >MODE_LAST) c=MODE_FIRST;
            break;
        case ENTER:
        case RIGHT:
            modeNO=c;
            if(modeNO==1)
              timeSet();
            return c;
            break;
        case LEFT:
            moneyChoose();
            break;
        case HOME:
            //main();
            break;
        case F12:
            if(mu==0)
            {
                backMusic(1);
                mu++;
            }
            else
            {
                backMusic(0);
                mu--;
            }
            break;
        }
        printf("\n");
    }
}

void worker()
{
    //system("cls");
    clear();
    gy=2;
    shoWord("title");

  int y=gy;
  int x=0;
  print_stuff(x, y);
    //system("pause");
    getch();
    //system("cls");
    //main();
}

void main_menu()
{
  int c=1/*紀錄目前選擇項目*/,k/*按鍵值存放*/;
        while(1)//KEYIN 處理
        {
          show(c);//選單畫面顯示(引入值為目前選擇項目)
          //mvprintw(0, 40, "c: %d", c);
          k=getch();
          switch(k)
          {
            case ERR: // nothing has been typed
            {
              static int i=0;
              mvprintw(0, 40, "no key: %d", i++);
              refresh();
              break;
            }
            case UP://按鍵上
            {
                c--;//目前選擇項目向上(減)
                if (c<MENU_FIRST) c=MENU_LAST;//邊界處裡
              break;
            }
            case DOWN://按鍵下
            {
                c++;//目前選擇項目向下(減加
                if (c >MENU_LAST) c=MENU_FIRST;//邊界處裡
              break;
            }
            /*選擇處理*/
            case RIGHT:
            case ENTER:
            {
              switch (c)
              {
                case 1:
                {
                    addGame();
                    break;
                }
                case 2:
                    readGame();
                    break;
                case 3:
                    highScore();
                    break;
                case 4:
                {
                    worker();
                    break;
                }
                case 5:
                {
                  gOut();
                  return;
                  break;
                }
              }
            }
          }
            #if 0
            switch(k)
            {
            case UP://按鍵上
                c--;//目前選擇項目向上(減)
                if (c<MENU_FIRST) c=MENU_LAST;//邊界處裡
                break;
            case DOWN://按鍵下
                c++;//目前選擇項目向下(減加
                if (c >MENU_LAST) c=MENU_FIRST;//邊界處裡
                break;
            /*選擇處理*/
            case ENTER:
            case RIGHT:
                switch (c)
                {
                case 5:
                    gOut();
                    exit(1);
                    break;
                case 4:
                    worker();
                    break;
                case 3:
                    highScore();
                    break;
                case 1:
                    addGame();
                    break;
                case 2:
                    readGame();
                    break;
                }
            case F12://音樂暫停/播放
                if(mu==0)
                {
                    backMusic(1);
                    mu++;
                }
                else
                {
                    backMusic(0);
                    mu--;
                }
                break;
            }
            system("cls");//螢幕清除
            #endif
        }
}

int main()
{
  setlocale(LC_ALL, "");

  initscr();
  keypad(stdscr,true);
  curs_set(0);
  //cbreak();
  //halfdelay(10);
  //nodelay(stdscr, true);
#if 1

#else
loserScreen();
  endwin();
  exit(0);
  draw_dice(2, 5, 3);
  getch();

  return 0;
  printw("123\n");
  refresh();
  printw("456\n");
  refresh();

  getch();
  exit(0);
#endif

#if 0
  noecho();
   curs_set(0);

#endif
  welcome();//歡迎畫面
    mapListIn();//地圖名稱清單讀入
    reSet();//全域變數初始化
  main_menu();
    /*選單迴圈*/
    //while(1)
    //{
    //}
end_loop:
  endwin();

    return 0;
}

void loserScreen()
{
    //system("mode con cols=80 lines=30");//視窗大小
    /*背景字體顏色*/
    textcolor(0,8);
    //system("cls");
    clear();
    gy=2;
    shoWord("noWinner");
    //printf("\n");
    //system("pause");
    getch();
    //main();
}

void winScreen(int a)
{
    //system("mode con cols=65 lines=30");//視窗大小
    /*背景字體顏色*/
    textcolor(0,8);
    //system("cls");
    clear();
    gy=2;
    shoWord("winer");
    //delay(800);
    sleep(1);
    gotoxy(46, gy++);
    printw("█");
    refresh();
    //delay(1200);
    sleep(1);
    gotoxy(50, gy);
    printw("█");
    refresh();
    //delay(1000);
    sleep(1);
    gotoxy(54, gy);
    printw("█");
    refresh();
    //delay(800);
    sleep(1);
    mvprintw(gy, 56, "%s 恭喜獲勝!", player[a]);
    pause_screen(0, gy+2, "press any key to continue ...");
    // 回主選單
    main_menu();
}

int save()
{
    textcolor(4,8);
    t3 = time(NULL);
    int x=43,y=13,i,j,k=1,u;
    char tem[3][100],txt[50];
    for(i=0; i<19; i++)
    {
        gotoxy(x-3,y-3+i);
        printw("                                    ");
    }
    gotoxy(x,y);
    printw("---------------------------------------");
    gotoxy(x,y+1);
    printw("|             遊 戲 存 檔             |");
    gotoxy(x,y+2);
    printw("---------------------------------------");
    gotoxy(x,y+3);
    printw("| No. |   地  圖  |   存  檔  時  間  |");
    gotoxy(x,y+4);
    printw("---------------------------------------");
    gotoxy(x,y+5);
    printw("|   1 |           |                   |");
    gotoxy(x,y+6);
    printw("|   2 |           |                   |");
    gotoxy(x,y+7);
    printw("|   3 |           |                   |");
    gotoxy(x,y+8);
    printw("|   4 |           |                   |");
    gotoxy(x,y+9);
    printw("|   5 |           |                   |");
    gotoxy(x,y+10);
    printw("---------------------------------------");
    gotoxy(x,y+11);
    printw("按ENTER 確定存檔/按 F7 退出存檔視窗");
    gotoxy(x+2,y+5);
    printw("->");
RE:
    for(i=k; i<6; i++)
    {
        sprintf(txt,".//data//save//show%d.dat",i);
        FILE *in = fopen(txt,"r");
        for(j=1; j<3; j++)
            fscanf(in,"%s",tem[j]);
        fclose(in);
        gotoxy(x+8,i+4+y);
        printw("%s",tem[1]);
        gotoxy(x+19,i+4+y);
        printw("%s",tem[2]);
    }
    while(1)
    {
        u=getch();
        switch(u)
        {
        case UP:
            k--;
            break;
        case DOWN:
            k++;
            break;
        case HOME:
        case LEFT:
            main();
            break;
        case ENTER:
        case RIGHT:
        {
            sprintf(txt,".//data//save//show%d.dat",k);
            FILE *in = fopen(txt,"wt");
#if 0
            SYSTEMTIME sys;
            GetLocalTime( &sys );
#endif
        time_t cur_time = time(NULL);
        struct tm * tm = localtime(&cur_time);
            fprintf(in,"%s\n",mapList[mapNO]);
            fprintf(in,"%4d/%02d/%02d_%02d:%02d:%02d\n", tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
            fclose(in);
            char txt2[100];
            sprintf(txt2,".//data//save//Value%d.dat",k);
            FILE *out = fopen(txt2,"wt");
            fprintf(out,"%d\n",1);
            int z;
            for(z=0; z<6; z++)
                fprintf(out,"%s\n",player[z]);
            for(z=0; z<6; z++)
                fprintf(out,"%d\n",allMoney[z]);
            for(z=0; z<6; z++)
                fprintf(out,"%d\n",status[z]);
            for(z=0; z<6; z++)
                fprintf(out,"%d\n",site[z]);
            for(z=0; z<6; z++)
                fprintf(out,"%d\n",ox[z]);
            for(z=0; z<6; z++)
                fprintf(out,"%d\n",oy[z]);
            fprintf(out,"%d\n",all);
            fprintf(out,"%d\n",comp);
            fprintf(out,"%d\n",mo);
            fprintf(out,"%d\n",star);
            fprintf(out,"%d\n",mapNO);
            fprintf(out,"%ld\n",useTime);
            fprintf(out,"%ld\n",limTime);
            fprintf(out,"%d\n",modeNO);
            for(z=0; z<6; z++)
                fprintf(out,"%d\n",canMove[z]);
            for(z=0; z<28; z++)
                fprintf(out,"%d\n",lev1[z]);
            for(z=0; z<28; z++)
                fprintf(out,"%d\n",lev2[z]);
            for(z=0; z<28; z++)
                fprintf(out,"%d\n",gp[z]);
            fprintf(out,"%d\n",loser);
            fclose(out);
            for(i=k; i<6; i++)
            {
                sprintf(txt,".//data//save//show%d.dat",i);
                FILE *in = fopen(txt,"r");
                for(j=1; j<3; j++)
                    fscanf(in,"%s",tem[j]);
                fclose(in);
                gotoxy(x+8,i+4+y);
                printw("%s",tem[1]);
                gotoxy(x+19,i+4+y);
                printw("%s",tem[2]);
            }
            refresh();
            //delay(1000);
            sleep(1);
            for(i=0; i<19; i++)
            {
                gotoxy(x-3,y-3+i);
                printw("                                                ");
            }
            textcolor(0,8);
            return 0;
            //goto RE;
            break;
        }
        case F7:
        {
            for(i=0; i<19; i++)
            {
                gotoxy(x-3,y-3+i);
                printw("                                                ");
            }
            t4 = time(NULL);
            t1=t1+(t4-t3);
            textcolor(0,8);
            return 0;
            break;
        }
        case F12:
            if(mu==0)
            {
                backMusic(1);
                mu++;
            }
            else
            {
                backMusic(0);
                mu--;
            }
            break;
       }
        if(k>5)k=1;
        if(k<1)k=5;
        for(j=1; j<6; j++)
        {
            gotoxy(x+2,y+4+j);
            printw("  ");
            if(k==j)
            {
                gotoxy(x+2,y+4+j);
                printw("->");
            }
        }
        refresh();
    }
}

void readGame()
{
    //system("cls");
    clear();
    mvprintw(1, 1 , "Loading...");
    int i,j,k=1,l=1,test=0,u;
    char txt[50],tem[3][100],txt2[100],txt3[100];
    while(1)
    {
back:
        clear();
        gy=2;
        shoWord("title");
        //printw("\n");
        shoWord("save/title");
        //printf("\n");
        for(i=1; i<6; i++)
        {
            sprintf(txt,".//data//save//show%d.dat",i);
            FILE *in = fopen(txt,"r");
#if 0
            if (!in) 
            {
              mvprintw(1,1, "file open fail %s", txt);
              getch();
              return;
            }
#endif
            for(j=1; j<3; j++)
                fscanf(in,"%s",tem[j]);
            fclose(in);
            gotoxy(7,i+10);
            printw("%s",tem[1]);
            gotoxy(21,i+10);
            printw("%s",tem[2]);
        }
        gotoxy(0,14);
        shoWord("save//explanation");
        gotoxy(2,10+k);
        printw("->");
        u=getch();
        switch(u)
        {
        case UP:
            k--;
            break;
        case DOWN:
            k++;
            break;
        case HOME:
        case LEFT:
            main();
            break;
        case ENTER:
        case RIGHT:
        {
            sprintf(txt2,".//data//save//Value%d.dat",k);
            FILE *out = fopen(txt2,"r");
            fscanf(out,"%d",&test);
            if(test!=1)
                goto back;
            int z;
            for(z=0; z<6; z++)
                fscanf(out,"%s",player[z]);
            for(z=0; z<6; z++)
                fscanf(out,"%d",&allMoney[z]);
            for(z=0; z<6; z++)
                fscanf(out,"%d",&status[z]);
            for(z=0; z<6; z++)
                fscanf(out,"%d",&site[z]);
            for(z=0; z<6; z++)
                fscanf(out,"%d",&ox[z]);
            for(z=0; z<6; z++)
                fscanf(out,"%d",&oy[z]);
            fscanf(out,"%d",&all);
            fscanf(out,"%d",&comp);
            fscanf(out,"%d",&mo);
            fscanf(out,"%d",&star);
            fscanf(out,"%d",&mapNO);
            fscanf(out,"%ld",&useTime);
            fscanf(out,"%ld",&limTime);
            fscanf(out,"%d",&modeNO);
            for(z=0; z<6; z++)
                fscanf(out,"%d",&canMove[z]);
            for(z=0; z<28; z++)
                fscanf(out,"%d",&lev1[z]);
            for(z=0; z<28; z++)
                fscanf(out,"%d",&lev2[z]);
            for(z=0; z<28; z++)
                fscanf(out,"%d",&gp[z]);
            fscanf(out,"%d",&loser);
            fclose(out);
            noRead=1;
            play();
            break;
        }
        case F8:
        {
            sprintf(txt,".//data//save//show%d.dat",k);
            FILE *in = fopen(txt,"wt");
            //SYSTEMTIME sys;
            //GetLocalTime( &sys );
        //time_t cur_time = time(NULL);
        //struct tm * tm = localtime(&cur_time);
            fprintf(in,"--\n");
            fprintf(in,"--\n");
            fclose(in);
            sprintf(txt,".//data//save//Value%d.dat",k);
            FILE *in4 = fopen(txt,"wt");
            fprintf(in4,"%d\n",0);
            fclose(in4);
            break;
        }
        case F12:
            if(mu==0)
            {
                backMusic(1);
                mu++;
            }
            else
            {
                backMusic(0);
                mu--;
            }
            break;
        }
        if(k>5)k=1;
        if(k<1)k=5;
        for(l=1; l<6; l++)
        {
            gotoxy(2,7+l);
            printw("  ");
            if(k==l)
            {
                gotoxy(2,7+l);
                printw("->");
            }
        }
        gotoxy(0,21);
        printw("%d %d %",k,all,comp);
    }
    //system("pause");
    getch();
}

void highScore()
{
    //system("mode con cols=45 lines=80");
    int i=0,j,ch,k;
    char name[50],map[50],ti[50];
        clear();
        gy=2;
        shoWord("title");
        //printf("\n");
        shoWord("highScore");
        //printf("\n");
        FILE *in=fopen(".//data//win//Value.dat","rt");
        while(in)
        {
            j=0;
            fscanf(in,"%d",&j);
            if(j==1)
            {
                i++;
                fscanf(in,"%s",&name);
                fscanf(in,"%s",&ti);
                fscanf(in,"%s",&map);
                gotoxy(0,10+i);
                printw("|           |                |             |");
                gotoxy(2,10+i);
                printw("%s",name);
                gotoxy(13,10+i);
                printw("%s",ti);
                gotoxy(31,10+i);
                printw("%s",map);
            }
            else
                break;
        }
        fclose(in);
        if(i==0)
        {
            gotoxy(0, 11);
            printw("|                 無紀錄                   |");
            printw("--------------------------------------------");
            printw("\n按ENTER回主選單...\n");
        }
        if(i>0)
        {
            mvprintw(11+i, 0, "--------------------------------------------");
            mvprintw(12+i, 0, "按 ENTER 鍵回主選單 或 F8 清除紀錄...");
        }
    while(1)
    {
        k=getch();
        switch(k)
        {
        case F8:
        {
          FILE *in2;
            in2=fopen(".//data//win//Value.dat","wt");
            j=0;
            i=0;
            fprintf(in2,"0");
            fclose(in2);
            break;
        }
        case F12:
            if(mu==0)
            {
                backMusic(1);
                mu++;
            }
            else
            {
                backMusic(0);
                mu--;
            }
            break;
        case ENTER:
            main_menu();
            break;
        }
    }
}

void home1()
{
    textcolor(4,8);
    int c=1,x=43,y=15,i,k;
    gotoxy(x,y);
    printw("-------------------------------------------");
    gotoxy(x,y+1);
    printw("|                                         |");
    gotoxy(x,y+2);
    printw("|                                         |");
    gotoxy(x,y+3);
    printw("|            你確定要離開嗎??             |");
    gotoxy(x,y+4);
    printw("|                                         |");
    gotoxy(x,y+5);
    printw("|                                         |");
    gotoxy(x,y+6);
    printw("| ->儲存後離開      直接離開       取消   |");
    gotoxy(x,y+7);
    printw("|                                         |");
    gotoxy(x,y+8);
    printw("|                                         |");
    gotoxy(x,y+9);
    printw("|                                         |");
    gotoxy(x,y+10);
    printw("-------------------------------------------");
    while(1)
    {
        k=getch();
        switch(k)
        {
        case RIGHT:
            c++;
            break;
        case LEFT:
            c--;
            break;
        case ENTER:
            if(c==2)
            {
                textcolor(0,8);
                main_menu();
                return;
            }

            if(c==3)
            {
                for(i=0; i<=13; i++)
                {
                    gotoxy(x,y+i);
                    printw("                                                    ");
                }
                textcolor(0,8);
                return;
            }
            if(c==1)
            {
                for(i=0; i<=13; i++)
                {
                    gotoxy(x,y+i);
                    printw("                                                    ");
                }
                textcolor(0,8);
                save();
                main_menu();
                return;
            }
            break;
        }
        if(c==4)c=1;
        if(c==0)c=3;
        if(c==1)
        {
            gotoxy(x,y+6);
            printw("| ->儲存後離開      直接離開       取消   |");
        }
        if(c==2)
        {
            gotoxy(x,y+6);
            printw("|   儲存後離開    ->直接離開       取消   |");
        }
        if(c==3)
        {
            gotoxy(x,y+6);
            printw("|   儲存後離開      直接離開     ->取消   |");
        }
    }
}


void pause_game()
{
    textcolor(4,8);
    t3 = time(NULL);
    backMusic(1);
    int x=50,y=15,i,m;
back:
    for(i=0; i<8; i++)
    {
        gotoxy(x,y-1+i);
        printw("                                 ");
    }
    gotoxy(x,y);
    printw("---------------------------------");
    gotoxy(x,y+1);
    printw("|                               |");
    gotoxy(x,y+2);
    printw("|  遊戲暫停中...                |");
    gotoxy(x,y+3);
    printw("|                               |");
    gotoxy(x,y+4);
    printw("|            按 ENTER 鍵開始 !! |");
    gotoxy(x,y+5);
    printw("|                               |");
    gotoxy(x,y+6);
    printw("---------------------------------");
    m=getch();
    if(m!=F8)
    {
        for(i=0; i<8; i++)
        {
            gotoxy(x,y-1+i);
            printw("                                   ");
        }
        t4 = time(NULL);
        if(mu==0)
            backMusic(0);
        t1=t1+(t4-t3)+(double)0.9999;
        textcolor(0,8);
        return;
    }
    goto back;
}

int itd()
{
    t3 = time(NULL);
    backMusic(1);
    int x=43,y=12,i,m;
back:
    for(i=0; i<16; i++)
    {
        gotoxy(x,y-1+i);
        printw("                                             ");
    }
    textcolor( 4 , 8);
    gotoxy(x,y);
    printw("---------------------------------------------");
    gotoxy(x,y+1);
    printw("|                 遊戲說明                   |");
    gotoxy(x,y+2);
    printw("|                                            |");
    gotoxy(x,y+3);
    printw("| 玩家先骰，到每個不同地域都可以買地         |");
    gotoxy(x,y+4);
    printw("|                                            |");
    gotoxy(x,y+5);
    printw("| 每次買地的等級都不同                       |");
    gotoxy(x,y+6);
    printw("|                                            |");
    gotoxy(x,y+7);
    printw("| 輪到電腦時，你不能操作(just_wait)          |");
    gotoxy(x,y+8);
    printw("|                                            |");
    gotoxy(x,y+9);
    printw("| 假如採到他方的地,就得依地價付錢            |");
    gotoxy(x,y+10);
    printw("|                                            |");
    gotoxy(x,y+11);
    printw("| 遊戲結束於時間歸零(限時模式)或破產(錢歸零) |");
    gotoxy(x,y+12);
    printw("|                                            |");
    gotoxy(x,y+13);
    printw("|                                            |");
    gotoxy(x,y+14);
    printw("| ..............按ENTER鍵離開................|");
    gotoxy(x,y+15);
    printw("---------------------------------------------");
    m=getch();
    if(m!=F8)
    {
        for(i=0; i<17; i++)
        {
            gotoxy(x,y-1+i);
            textcolor( 0 , 8);
            printw("                                                     ");
        }
        t4 = time(NULL);
        if(mu==0)
            backMusic(0);
        t1=t1+(t4-t3)-2;
        return 0;
    }
    else
        goto back;
}
